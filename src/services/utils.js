import _ from 'lodash';
import moment from 'moment';

export const normalizeValue = (value, minValue = 0, maxValue = 100) => {
  if (value <= minValue) {
    return minValue
  }

  if (value >= maxValue) {
    return maxValue
  }

  return value
}

export const makeHttpClient = (VueAuth, params) => {
  return VueAuth.options.http._http.call(VueAuth, params)
}

/**
 * Deep diff between two object, using lodash
 * @param  {Object} object Object compared
 * @param  {Object} base   Object to compare with
 * @return {Object}        Return a new object who represent the diff
 */
export const difference = (object, base) => {
  function changes(object, base) {
    return _.transform(object, function (result, value, key) {
      if (value != base[key]) {
        result[key] = (_.isObject(value) && _.isObject(base[key])) ? changes(value, base[key]) : value;
      }
    });
  }
  return changes(object, base);
}

export const formatDate =  (value, fmt) => {
  if (value == null) return ''
  fmt = (typeof fmt == 'undefined') ? 'DD-MM-YYYY HH:mm' : fmt
  return moment.utc(value, 'YYYY-MM-DD HH:mm').local().format(fmt)
}

export const formatDecimal =  (value, dLength) => {
  dLength = (typeof dLength == 'undefined') ? 2 : dLength
  return parseFloat(value).toFixed(dLength);
}

export const formatNumber =  (value, dLength) => {
  dLength = (typeof dLength == 'undefined') ? 2 : dLength
  return value ? value.toLocaleString() : value;
}

export const ellipseString = (
  address = "",
  width = 10
) =>  {
  if(!address) return "";
  return `${address.slice(0, width)}...${address.slice(-width)}`;
}