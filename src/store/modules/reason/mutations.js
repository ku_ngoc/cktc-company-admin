import { FETCH_REASON, GET_DETAIL_REASON } from './constants'
export default {
  [FETCH_REASON](state, payload) {
    console.log(`reason::mutation::FETCH_REASON`, payload);
    state.list = [].concat(payload)
  },
  [GET_DETAIL_REASON](state, payload) {
    console.log(`service::mutation::GET_DETAIL_REASON`, payload);
    state.detail = Object.assign({}, payload)
  },
}
