import { FETCH_REASON, GET_DETAIL_REASON } from './constants'
import { makeHttpClient, difference } from "../../../services/utils";
import Vue from 'vue'

export default {
  fetchReason({ commit }, params) {
    makeHttpClient(params.auth, {
      url: "/reasons",
      methods: "get",
      success: params.success
    }).then(res => {
      console.log(`action::fetchReason`, res);
      commit(FETCH_REASON, res.data.data);
    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  getDetailReason({ commit }, params) {
    makeHttpClient(params.auth, {
      url: `/reasons/${params.id}`,
      methods: "get",
      success: params.success
    }).then(res => {
      console.log(`action::fetchReason`, res);
      commit(GET_DETAIL_REASON, res.data.data);
    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  updateReason({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/reason/${params.id}`,
      method: "put",
      data: params.data,
      success: params.success,
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  saveReason({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/reason`,
      method: "post",
      data: params.data,
      success: params.success
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  deleteReason({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/reason/${params.id}`,
      method: "delete",
      success: params.success
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
}

