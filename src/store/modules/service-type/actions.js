import { FETCH_SERVICE_TYPES, GET_DETAIL_SERVICE_TYPES } from './constants'
import { makeHttpClient, difference } from "../../../services/utils";
import Vue from 'vue'

export default {
  fetchServiceTypes({ commit }, params) {
    makeHttpClient(params.auth, {
      url: "/service-types",
      methods: "get",
      success: params.success
    }).then(res => {
      console.log(`action::fetchServiceTypes`, res);
      commit(FETCH_SERVICE_TYPES, res.data.data);
    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  getDetailServiceType({ commit }, params) {
    makeHttpClient(params.auth, {
      url: `/service-types/${params.code}`,
      methods: "get",
      success: params.success
    }).then(res => {
      console.log(`action::fetchServiceTypes`, res);
      commit(GET_DETAIL_SERVICE_TYPES, res.data.data);
    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  updateServiceType({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/service-type/${params.id}`,
      method: "put",
      data: params.data,
      success: params.success,
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  saveServiceType({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/service-type`,
      method: "post",
      data: params.data,
      success: params.success
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  deleteServiceType({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/service-type/${params.id}`,
      method: "delete",
      success: params.success
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
}

