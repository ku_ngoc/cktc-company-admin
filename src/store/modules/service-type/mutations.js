import { FETCH_SERVICE_TYPES, GET_DETAIL_SERVICE_TYPES } from './constants'
export default {
  [FETCH_SERVICE_TYPES](state, payload) {
    console.log(`service-type::mutation::FETCH_SERVICE_TYPES`, payload);
    state.list = [].concat(payload)
  },
  [GET_DETAIL_SERVICE_TYPES](state, payload) {
    console.log(`service::mutation::GET_DETAIL_SERVICE_TYPES`, payload);
    state.detail = Object.assign({}, payload)
  },
}
