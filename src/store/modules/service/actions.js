import { FETCH_SERVICES, GET_DETAIL_SERVICE } from './constants'
import { makeHttpClient, difference } from "../../../services/utils";
import Vue from 'vue'

export default {
  fetchServices({ commit }, params) {
    makeHttpClient(params.auth, {
      url: "/services",
      methods: "get",
      success: params.success
    }).then(res => {
      console.log(`action::fetchServices`, res);
      commit(FETCH_SERVICES, res.data.data);
    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  getDetailService({ commit }, params) {
    makeHttpClient(params.auth, {
      url: `/services/${params.slug}`,
      methods: "get",
      success: params.success
    }).then(res => {
      console.log(`action::fetchServices`, res);
      commit(GET_DETAIL_SERVICE, res.data.data);
    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  updateService({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/service/${params.id}`,
      method: "put",
      data: params.data,
      success: params.success,
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  saveService({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/service`,
      method: "post",
      data: params.data,
      success: params.success
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  deleteService({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/service/${params.id}`,
      method: "delete",
      success: params.success
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
}

