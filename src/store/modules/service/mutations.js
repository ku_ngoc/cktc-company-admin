import { FETCH_SERVICES, GET_DETAIL_SERVICE } from './constants'
export default {
  [FETCH_SERVICES](state, payload) {
    console.log(`service::mutation::FETCH_SERVICES`, payload);
    state.list = [].concat(payload)
  },
  [GET_DETAIL_SERVICE](state, payload) {
    console.log(`service::mutation::GET_DETAIL_SERVICE`, payload);
    state.detail = Object.assign({}, payload)
  },
}
