import { FETCH_SETTINGS, GET_DETAIL_SETTING } from './constants'
import { makeHttpClient, difference } from "../../../services/utils";
import Vue from 'vue'

export default {
  fetchSettings({ commit }, params) {
    makeHttpClient(params.auth, {
      url: "/settings",
      methods: "get",
      success: params.success
    }).then(res => {
      console.log(`action::fetchSettings`, res);
      commit(FETCH_SETTINGS, res.data.data);
    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  getDetailSetting({ commit }, params) {
    makeHttpClient(params.auth, {
      url: `/settings/${params.code}`,
      methods: "get",
      success: params.success
    }).then(res => {
      console.log(`action::fetchSettings`, res);
      commit(GET_DETAIL_SETTING, res.data.data);
    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  updateSetting({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/setting/${params.id}`,
      method: "put",
      data: params.data,
      success: params.success,
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  saveSetting({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/setting`,
      method: "post",
      data: params.data,
      success: params.success
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  deleteSetting({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/setting/${params.id}`,
      method: "delete",
      success: params.success
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
}

