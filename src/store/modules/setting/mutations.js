import { FETCH_SETTINGS, GET_DETAIL_SETTING } from './constants'
export default {
  [FETCH_SETTINGS](state, payload) {
    console.log(`service::mutation::FETCH_SETTINGS`, payload);
    state.list = [].concat(payload)
  },
  [GET_DETAIL_SETTING](state, payload) {
    console.log(`service::mutation::GET_DETAIL_SETTING`, payload);
    state.detail = Object.assign({}, payload)
  },
}
