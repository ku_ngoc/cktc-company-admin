import { FETCH_COMPANIES, GET_DETAIL_COMPANY } from './constants'
export default {
  [FETCH_COMPANIES](state, payload) {
    console.log(`company::mutation::FETCH_COMPANIES`, payload);
    state.list = [].concat(payload)
  },
  [GET_DETAIL_COMPANY](state, payload) {
    console.log(`company::mutation::GET_DETAIL_COMPANY`, payload);
    state.detail = Object.assign({}, payload)
  },
}
