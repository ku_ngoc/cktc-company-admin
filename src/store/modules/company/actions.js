import { FETCH_COMPANIES, GET_DETAIL_COMPANY } from './constants'
import { makeHttpClient, difference } from "../../../services/utils";
import Vue from 'vue'

export default {
  fetchCompanies({ commit }, params) {
    makeHttpClient(params.auth, {
      url: "/companies",
      methods: "get",
      success: params.success
    }).then(res => {
      console.log(`action::fetchCompanies`, res);
      commit(FETCH_COMPANIES, res.data.data);
    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  getDetailCompany({ commit }, params) {
    makeHttpClient(params.auth, {
      url: `/companies/${params.slug}`,
      methods: "get",
      success: params.success
    }).then(res => {
      console.log(`action::fetchCompanies`, res);
      commit(GET_DETAIL_COMPANY, res.data.data);
    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  updateCompany({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/company/${params.id}`,
      method: "put",
      data: params.data,
      success: params.success,
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  saveCompany({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/company`,
      method: "post",
      data: params.data,
      success: params.success
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  deleteCompany({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/company/${params.id}`,
      method: "delete",
      success: params.success
    }).then(res => {
      params.success()
    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
}

