import { FETCH_AREAS, GET_DETAIL_AREA } from './constants'
export default {
  [FETCH_AREAS](state, payload) {
    console.log(`service::mutation::FETCH_AREAS`, payload);
    state.list = [].concat(payload)
  },
  [GET_DETAIL_AREA](state, payload) {
    console.log(`service::mutation::GET_DETAIL_AREA`, payload);
    state.detail = Object.assign({}, payload)
  },
}
