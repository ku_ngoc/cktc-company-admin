import { FETCH_AREAS, GET_DETAIL_AREA } from './constants'
import { makeHttpClient, difference } from "../../../services/utils";
import Vue from 'vue'

export default {
  fetchAreas({ commit }, params) {
    makeHttpClient(params.auth, {
      url: "/areas",
      methods: "get",
      success: params.success
    }).then(res => {
      console.log(`action::fetchAreas`, res);
      commit(FETCH_AREAS, res.data.data);
    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  getDetailArea({ commit }, params) {
    makeHttpClient(params.auth, {
      url: `/areas/${params.code}`,
      methods: "get",
      success: params.success
    }).then(res => {
      console.log(`action::fetchAreas`, res);
      commit(GET_DETAIL_AREA, res.data.data);
    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  updateArea({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/area/${params.id}`,
      method: "put",
      data: params.data,
      success: params.success,
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  saveArea({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/area`,
      method: "post",
      data: params.data,
      success: params.success
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  deleteArea({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/area/${params.id}`,
      method: "delete",
      success: params.success
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
}

