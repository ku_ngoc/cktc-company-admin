import { FETCH_BUSINESS, GET_DETAIL_BUSINESS } from './constants'
export default {
  [FETCH_BUSINESS](state, payload) {
    console.log(`business::mutation::FETCH_BUSINESS`, payload);
    state.list = [].concat(payload)
  },
  [GET_DETAIL_BUSINESS](state, payload) {
    console.log(`service::mutation::GET_DETAIL_BUSINESS`, payload);
    state.detail = Object.assign({}, payload)
  },
}
