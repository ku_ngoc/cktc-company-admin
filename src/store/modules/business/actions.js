import { FETCH_BUSINESS, GET_DETAIL_BUSINESS } from './constants'
import { makeHttpClient, difference } from "../../../services/utils";
import Vue from 'vue'

export default {
  fetchBusiness({ commit }, params) {
    makeHttpClient(params.auth, {
      url: "/business",
      methods: "get",
      success: params.success
    }).then(res => {
      console.log(`action::fetchBusiness`, res);
      commit(FETCH_BUSINESS, res.data.data);
    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  getDetailBusiness({ commit }, params) {
    makeHttpClient(params.auth, {
      url: `/business/${params.id}`,
      methods: "get",
      success: params.success
    }).then(res => {
      console.log(`action::fetchBusiness`, res);
      commit(GET_DETAIL_BUSINESS, res.data.data);
    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  updateBusiness({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/business/${params.id}`,
      method: "put",
      data: params.data,
      success: params.success,
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  saveBusiness({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/business`,
      method: "post",
      data: params.data,
      success: params.success
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  deleteBusiness({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/business/${params.id}`,
      method: "delete",
      success: params.success
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
}

