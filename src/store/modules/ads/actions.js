import { FETCH_ADS, GET_DETAIL_ADS } from './constants'
import { makeHttpClient, difference } from "../../../services/utils";
import Vue from 'vue'

export default {
  fetchAds({ commit }, params) {
    makeHttpClient(params.auth, {
      url: "/ads",
      methods: "get",
      success: params.success
    }).then(res => {
      console.log(`action::fetchAds`, res);
      commit(FETCH_ADS, res.data.data);
    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  getDetailAds({ commit }, params) {
    makeHttpClient(params.auth, {
      url: `/ads/${params.id}`,
      methods: "get",
      success: params.success
    }).then(res => {
      console.log(`action::fetchAds`, res);
      commit(GET_DETAIL_ADS, res.data.data);
    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  updateAds({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/ads/${params.id}`,
      method: "put",
      data: params.data,
      success: params.success,
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  saveAds({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/ads`,
      method: "post",
      data: params.data,
      success: params.success
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  deleteAds({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/ads/${params.id}`,
      method: "delete",
      success: params.success
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
}

