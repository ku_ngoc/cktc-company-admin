import { FETCH_ADS, GET_DETAIL_ADS } from './constants'
export default {
  [FETCH_ADS](state, payload) {
    console.log(`ads::mutation::FETCH_ADS`, payload);
    state.list = [].concat(payload)
  },
  [GET_DETAIL_ADS](state, payload) {
    console.log(`service::mutation::GET_DETAIL_ADS`, payload);
    state.detail = Object.assign({}, payload)
  },
}
