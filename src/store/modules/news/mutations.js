import { FETCH_NEWS, GET_DETAIL_NEWS } from './constants'
export default {
  [FETCH_NEWS](state, payload) {
    console.log(`news::mutation::FETCH_NEWS`, payload);
    state.list = [].concat(payload)
  },
  [GET_DETAIL_NEWS](state, payload) {
    console.log(`news::mutation::GET_DETAIL_NEWS`, payload);
    state.detail = Object.assign({}, payload)
  },
}
