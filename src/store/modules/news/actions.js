import { FETCH_NEWS, GET_DETAIL_NEWS } from './constants'
import { makeHttpClient, difference } from "../../../services/utils";
import Vue from 'vue'

export default {
  fetchNews({ commit }, payload) {
    makeHttpClient(payload.auth, {
      url: "/news",
      params: Object.assign({}, payload.params, {limit: 1000}),
      methods: "get",
      success: payload.success
    }).then(res => {
      console.log(`action::fetchNews`, res);
      commit(FETCH_NEWS, res.data.data);
    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  getDetailNews({ commit }, params) {
    makeHttpClient(params.auth, {
      url: `/news/${params.slug}`,
      methods: "get",
      success: params.success
    }).then(res => {
      console.log(`action::fetchNews`, res);
      commit(GET_DETAIL_NEWS, res.data.data);
    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  updateNews({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/news/${params.id}`,
      method: "put",
      data: params.data,
      success: params.success,
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  saveNews({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/news`,
      method: "post",
      data: params.data,
      success: params.success
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
  deleteNews({ commit }, params) {
    return makeHttpClient(params.auth, {
      url: `/news/${params.id}`,
      method: "delete",
      success: params.success
    }).then(res => {

    }).catch(function (res) {
      Vue.notify({
        type: 'error',
        group: 'app',
        text: res.response.data.message || res.message
      });
    });
  },
}

