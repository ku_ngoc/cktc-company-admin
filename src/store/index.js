import Vue from 'vue'
import Vuex from 'vuex'
import VuexI18n from 'vuex-i18n' // load vuex i18n module
import app from './modules/app'
import service from './modules/service'
import ServiceType from './modules/service-type'
import area from './modules/area'
import setting from './modules/setting'
import business from './modules/business'
import reason from './modules/reason'
import company from './modules/company'
import ads from './modules/ads'
import news from './modules/news'

export const strict = false

import * as getters from './getters'

Vue.use(Vuex)

const store = new Vuex.Store({
  strict: true, // process.env.NODE_ENV !== 'production',
  getters,
  modules: {
    app,
    service,
    'service-type': ServiceType,
    area,
    setting,
    business,
    reason,
    company,
    news,
    ads
  },
  state: {},
  mutations: {},
})

Vue.use(VuexI18n.plugin, store)

export default store
