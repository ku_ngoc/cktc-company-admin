import Vue from 'vue'
import Router from 'vue-router'
import AuthLayout from '../components/auth/AuthLayout'
import AppLayout from '../components/admin/AppLayout'
import { AccessControl } from "role-acl";

Vue.use(Router)

const EmptyParentComponent = {
  template: '<router-view></router-view>',
}

export default new Router({
  mode: process.env.VUE_APP_ROUTER_MODE_HISTORY === 'true' ? 'history' : 'hash',
  routes: [
    {
      path: '/',
      redirect: { name: 'companies' },
    },
    {
      path: '/auth',
      component: AuthLayout,
      children: [
        {
          name: 'login',
          path: 'login',
          component: () => import('../components/auth/login/Login.vue'),
        },
        {
          path: '',
          redirect: { name: 'login' },
        },
      ],
    },
    {
      path: '/404',
      name: '404',
      component: () => import('../components/pages/404-pages/VaPageNotFoundLargeText.vue'),
    },
    {
      path: '/403',
      name: '403',
      component: () => import('../components/pages/403-pages/VaPageNotPermissionLargeText.vue'),
    },
    {
      name: 'Admin',
      path: '/admin',
      component: AppLayout,
      meta: { auth: true },
      afterEnter: async (to, from, next) => {
        try {
          const acl = new AccessControl([].concat(Vue.auth.user().grants));
          const permission = await acl
            .can(Vue.auth.user().roles)
            .execute("GET")
            .on(`/${to.name}`);
          if (permission.granted) return next()
          return next('/403')
        } catch (error) {
          next('/403')
        }
      },
      children: [
        {
          name: 'companies',
          path: 'companies',
          component: () => import('../components/company/CompanyList.vue'),
        }, {
          name: 'edit.company',
          path: 'edit-company/:slug',
          component: () => import('../components/company/CompanyEdit.vue'),
        }, {
          name: 'create.company',
          path: 'create-company',
          component: () => import('../components/company/CompanyCreate.vue'),
        },
        {
          meta: {
            auth: {
              roles: 'admin'
            }
          },
          name: 'services',
          path: 'services',
          component: () => import('../components/service/ServiceList.vue'),
        }, {
          meta: {
            auth: {
              roles: 'admin'
            }
          },
          name: 'edit.service',
          path: 'edit-service/:slug',
          component: () => import('../components/service/ServiceEdit.vue'),
        }, {
          name: 'create.service',
          path: 'create-service',
          component: () => import('../components/service/ServiceCreate.vue'),
        },
        {
          name: 'areas',
          path: 'areas',
          component: () => import('../components/area/AreaList.vue'),
        }, {
          name: 'edit.area',
          path: 'edit-area/:code',
          component: () => import('../components/area/AreaEdit.vue'),
        }, {
          name: 'create.area',
          path: 'create-area',
          component: () => import('../components/area/AreaCreate.vue'),
        },
        {
          name: 'service-types',
          path: 'service-types',
          component: () => import('../components/service-type/ServiceTypeList.vue'),
        }, {
          name: 'edit.service-type',
          path: 'edit-service-type/:code',
          component: () => import('../components/service-type/ServiceTypeEdit.vue'),
        }, {
          name: 'create.service-type',
          path: 'create-service-type',
          component: () => import('../components/service-type/ServiceTypeCreate.vue'),
        },
        {
          name: 'settings',
          path: 'settings',
          component: () => import('../components/setting/SettingList.vue'),
        }, {
          name: 'edit.setting',
          path: 'edit-setting/:code',
          component: () => import('../components/setting/SettingEdit.vue'),
        }, {
          name: 'create.setting',
          path: 'create-setting',
          component: () => import('../components/setting/SettingCreate.vue'),
        },
        {
          name: 'business',
          path: 'business',
          component: () => import('../components/business/BusinessList.vue'),
        }, {
          name: 'edit.business',
          path: 'edit-business/:id',
          component: () => import('../components/business/BusinessEdit.vue'),
        }, {
          name: 'create.business',
          path: 'create-business',
          component: () => import('../components/business/BusinessCreate.vue'),
        },
        {
          name: 'reason',
          path: 'reason',
          component: () => import('../components/reason/ReasonList.vue'),
        }, {
          name: 'edit.reason',
          path: 'edit-reason/:id',
          component: () => import('../components/reason/ReasonEdit.vue'),
        }, {
          name: 'create.reason',
          path: 'create-reason',
          component: () => import('../components/reason/ReasonCreate.vue'),
        },
        {
          name: 'ads',
          path: 'ads',
          component: () => import('../components/ads/AdsList.vue'),
        }, {
          name: 'edit.ads',
          path: 'edit-ads/:id',
          component: () => import('../components/ads/AdsEdit.vue'),
        }, {
          name: 'create.ads',
          path: 'create-ads',
          component: () => import('../components/ads/AdsCreate.vue'),
        },
        {
          name: 'news',
          path: 'news',
          component: () => import('../components/news/NewsList.vue'),
        }, {
          name: 'edit.news',
          path: 'edit-news/:id',
          component: () => import('../components/news/NewsEdit.vue'),
        }, {
          name: 'create.news',
          path: 'create-news',
          component: () => import('../components/news/NewsCreate.vue'),
        },
      ],
    },
    {
      path: '**',
      redirect: { name: '404' },
    },
  ],
})
