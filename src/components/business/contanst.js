import { formatDecimal, formatNumber } from '../../services/utils';

export const FIELDS = [
    {
        name: "link",
        title: "Đường dẫn"
    },
    {
        name: "title",
        title: "Tiêu đề"
    },
    {
        name: "description",
        title: "Mô tả",
        width: '400px',
        dataClass: "description"
    }
];