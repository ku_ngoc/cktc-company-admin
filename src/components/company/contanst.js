import { formatDecimal, formatNumber } from '../../services/utils';

export const FIELDS = [
    {
        name: "MaSoThue",
        title: "Mã số thuế"
    },
    {
        name: "Title",
        title: "Tên công ty",
    },
    {
        name: "DiaChiCongTy",
        title: "Địa chỉ công ty",
    },
];