import { formatDecimal, formatNumber } from '../../services/utils';

export const FIELDS = [
    {
        name: "name",
        title: "Tên"
    },
    {
        name: "code",
        title: "Mã"
    }
];