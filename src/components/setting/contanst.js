import { formatDecimal, formatNumber } from '../../services/utils';

export const FIELDS = [
    {
        name: "name",
        title: "Tên",
    },
    {
        name: "code",
        title: "Khoá"
    },
    {
        name: "value",
        width: '400px',
        title: "Giá trị",
        dataClass: "value"
    }
];