export const navigationRoutes = {
  root: {
    name: '/',
    path: "/",
    displayName: 'navigationRoutes.home',
  },
  routes: [
    {
      name: 'companies',
      resource: "company",
      path: "/admin/companies",
      displayName: 'menu.companies',
      children: false,
      meta: {
        iconClass: 'fa fa-table',
      },
    },
    {
      name: 'news',
      resource: "news",
      path: "/admin/news",
      displayName: 'menu.news',
      children: false,
      meta: {
        iconClass: 'fa fa-table',
      },
    },
    {
      name: 'ads',
      resource: "ads",
      path: "/admin/ads",
      displayName: 'menu.ads',
      children: false,
      meta: {
        iconClass: 'fa fa-table',
      },
    },
    {
      name: 'services',
      resource: "service",
      path: "/admin/services",
      displayName: 'menu.services',
      children: false,
      meta: {
        iconClass: 'fa fa-table',
        roles: 'admin'
      },
    },
    {
      name: 'areas',
      resource: "area",
      path: "/admin/areas",
      displayName: 'menu.areas',
      children: false,
      meta: {
        iconClass: 'fa fa-table',
      },
    },
    {
      name: 'service-types',
      resource: "service-type",
      path: "/admin/service-types",
      displayName: 'menu.serviceTypes',
      children: false,
      meta: {
        iconClass: 'fa fa-table',
      },
    },
    {
      name: 'business',
      resource: "business",
      path: "/admin/business",
      displayName: 'menu.business',
      children: false,
      meta: {
        iconClass: 'fa fa-table',
      },
    },
    {
      name: 'reason',
      resource: "reason",
      path: "/admin/reason",
      displayName: 'menu.reason',
      children: false,
      meta: {
        iconClass: 'fa fa-table',
      },
    },
    {
      name: 'settings',
      resource: "setting",
      path: "/admin/settings",
      displayName: 'menu.settings',
      children: false,
      meta: {
        iconClass: 'fa fa-table',
      },
    },
  ],
}
