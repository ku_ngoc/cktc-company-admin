export const FIELDS = [
    {
        name: "title",
        title: "Tiêu đề"
    },
    {
        name: "slug",
        title: "Đường dẫn"
    },
    {
        name: "description",
        title: "Giới thiệu sơ lược"
    },
    {
        name: "number_view",
        title: "Lược đọc"
    }
];