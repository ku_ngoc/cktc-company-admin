import { formatDecimal, formatNumber } from '../../services/utils';

export const FIELDS = [
    {
        name: "icon",
        title: "Biểu tượng"
    },
    {
        name: "title",
        title: "Tiêu đề"
    },
    {
        name: "description",
        title: "Mô tả",
        width: '400px',
        dataClass: "description"
    }
];