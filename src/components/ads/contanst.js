import { formatDecimal, formatNumber } from '../../services/utils';

export const FIELDS = [
    {
        name: "title",
        title: "Tiêu đề"
    },
    {
        name: "url",
        title: "Đường dẫn"
    },
    {
        name: "__slot:position",
        title: "Vị Trí Quảng Cáo"
    },
    {
        name: "order",
        title: "Thứ Tự Quảng Cáo"
    },
    {
        name: "description",
        title: "Mô tả",
        width: '400px',
        dataClass: "description"
    },
];

export const POSITION_LIST = [
    {
        text: "Trái",
        id: 1
    },
    {
        text: "Phải",
        id: 2
    }
]