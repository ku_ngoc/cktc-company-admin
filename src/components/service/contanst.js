import { formatDecimal, formatNumber } from '../../services/utils';

export const FIELDS = [
    {
        name: "slug",
        title: "Đường dẫn"
    },
    {
        name: "__slot:service_type",
        title: "loại dịch vụ"
    },
    
    {
        name: "title",
        title: "Tiêu đề",
    },
];