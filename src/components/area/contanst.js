import { formatDecimal, formatNumber } from '../../services/utils';

export const FIELDS = [
    {
        name: "name",
        title: "Tên"
    },
    {
        name: "code",
        title: "Mã"
    },
    {
        name: "service_fee",
        title: "Phí dịch vụ",
    },
    {
        name: "tax_fee",
        title: "Phí khai báo thuế ban đầu",
    },
];