// Polyfills
import 'es6-promise/auto'
import 'babel-polyfill'
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import { ColorThemePlugin } from 'vuestic-ui/src/services/ColorThemePlugin'
import store from '../store/index'
import router from '../router/index'
import VuesticPlugin from 'vuestic-ui/src/components/vuestic-plugin'
import '../i18n/index'
import VeeValidate, {Validator} from 'vee-validate'

import '../metrics'

import axios from 'axios';
import VueAxios from 'vue-axios';
Vue.use(VueAxios, axios);

Vue.axios.defaults.baseURL = `${process.env.VUE_APP_API_URL}/api/v1`;

Vue.router = router;
Vue.use(require('@websanova/vue-auth'), {
  auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
  http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
  router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
  authRedirect: '/auth/login',
  refreshData: {
    enabled: false
  },
})

import Fragment from 'vue-fragment'
Vue.use(Fragment.Plugin)

Validator.extend('exist', {
  getMessage: field => 'The ' + field + ' value has existed.',
  validate: (value, list) => {
    return !(list || []).some(item => item == value);
  }
});

Validator.extend('unique', {
  getMessage: field => 'The ' + field + ' value has existed.',
  validate: (value, [origin_level, ...list]) => {
    if(value == origin_level) return true;
    return !(Object.values(list) || []).some(item => item == value);
  }
});

Vue.use(VeeValidate, {
  inject: true,
  fieldsBagName: 'veeFields',
  // This is not required but avoids possible naming conflicts
  errorBagName: 'veeErrors'
});

Vue.use(VuesticPlugin)

Vue.use(ColorThemePlugin,
  {
    // Add or change theme colors here
    themes: {
      // primary: '#66e5b5',
      // blurple: '#7289DA',
      'dark-green': '#1b9a7c'
    },
  })

import Notifications from 'vue-notification'
Vue.use(Notifications)

import VueLodash from 'vue-lodash'
Vue.use(VueLodash)

import VueSweetalert2 from 'vue-sweetalert2';

// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';
Vue.use(VueSweetalert2);

import CKEditor from '@ckeditor/ckeditor5-vue';
Vue.use( CKEditor );

router.beforeEach((to, from, next) => {
  store.commit('setLoading', true)
  next()
})

router.afterEach((to, from) => {
  store.commit('setLoading', false)
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
})
