// export const ACTION_FIELD = {
//     name: "actions",
//     title: "",
//     dataClass: "text-right"
// };

export const ACTION_FIELD = {
    name: "__slot:actions",
    dataClass: "actions"
};

export const PER_PAGE_OPTIONS = ["10", "20", "50", "100"]
export const DEFAULT_PER_PAGE = "10"