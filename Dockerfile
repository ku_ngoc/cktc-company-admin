FROM node:10.16.3-alpine

# RUN set -xe \
    # && apk add --no-cache bash git openssh \
    # && npm install -g npm \
    # && git --version && bash --version && ssh -V && npm -v && node -v && yarn -v

RUN npm install -g pm2 nodemon serve
# Fix Cannot find module '/root/.npm/_npx/24/lib/node_modules/sequelize-cli/node_modules/core-js/scripts/postinstall'
# RUN echo "unsafe-perm = true" >> ~/.npmrc
#

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json .
RUN npm install

COPY . .

ARG ENV_PREFIX
ADD .env.${ENV_PREFIX} .env
RUN npm run build

EXPOSE 3002
CMD npm run production
